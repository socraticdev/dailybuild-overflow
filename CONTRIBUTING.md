# how to contribute

- be a regular on #dailybuild channel (irc Rizon network)
- pick a listed issue and submit a merge request pertaining exactly to this issue
    - or
- discuss, on the channel, about adding or fixing something (socraticDev needs to be aware of your effort)
