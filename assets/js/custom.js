$(function () {
  $(".userProjectsSection p").on({
    mouseenter: function () {
      $(this).css("background", "white");
      $(this).css("cursor", "pointer")
      $(this).children("a").css("color", "black");
    },
    mouseleave: function () {
        $(this).css("background", "none");
        $(this).css("cursor", "default")
        $(this).children("a").css("color", "white");
    },
  });

  $(".userProjectsSection p").click(function() {
    $(this).children("a")[0].click();
  })
});
